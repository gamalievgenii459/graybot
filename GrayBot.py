import cv2
import telebot
import os

bot = telebot.TeleBot("token")


@bot.message_handler(content_types=['photo'])
def handle_photo(message):
    try:
        file_info = bot.get_file(message.photo[len(message.photo)-1].file_id)
        downloaded_file = bot.download_file(file_info.file_path)

        src = os.getcwd()+'/img/' + file_info.file_path
        with open(src, 'wb') as new_file:
            new_file.write(downloaded_file)

        img = cv2.imread(os.getcwd()+"/img/"+file_info.file_path,0)
        'папка document'

        cv2.imwrite(os.getcwd()+'/gray/' + file_info.file_path, img)
        'папка document'

        bot.send_photo(message.from_user.id, open(os.getcwd()+'/gray/' + file_info.file_path, 'rb'))

    except Exception as e:
        bot.reply_to(message, e)

if __name__ == '__main__':
    bot.polling(none_stop=True)
